from setuptools import setup

setup(
    name='snipper',
    version='0.0.8',
    packages=['snipper'],
    include_package_data=True
)


