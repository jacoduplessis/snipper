class Config:
    ALLOWED_SOURCES = []

    # the max width of the resized image
    # use 0 for no max width
    # if the original image is larger than MAX_WIDTH x MAX_HEIGHT,
    # it is proportionally resized to MAX_WIDTH x MAX_HEIGHT
    # MAX_WIDTH = 800
    MAX_WIDTH = 0
    MAX_HEIGHT = 0
    MAX_PIXELS = 75e6
    MIN_WIDTH = 1
    MIN_HEIGHT = 1
    QUALITY = 80
    PROGRESSIVE_JPEG = True
    USE_BLACKLIST = False
    ALLOW_ANIMATED_GIFS = False
    RESPECT_ORIENTATION = True
    PILLOW_JPEG_SUBSAMPLING = None
    PILLOW_JPEG_QTABLES = None
    PILLOW_RESAMPLING_FILTER = 'LANCZOS'
    META_CALLBACK_NAME = None
    # META_CALLBACK_NAME = 'thumbor_callback' # for jsonp

    # the max height of the resized image
    # use 0 for no max height
    # if the original image is larger than MAX_WIDTH x MAX_HEIGHT,
    # it is proportionally resized to MAX_WIDTH x MAX_HEIGHT
    # MAX_HEIGHT = 600

    # the quality of the generated image
    # this option can vary widely between
    # imaging engines and works only on jpeg images

    WEBP_QUALITY = 80

    # Automatically converts images to WebP if Accepts header present
    AUTO_WEBP = False

    # Specify the ratio between 1in and 1px for SVG images. This is only used when
    # rasterizing SVG images having their size units in cm or inches.
    # SVG_DPI = 150

    # Preserves exif information in generated images. Increases image size in kbytes, use with caution.
    PRESERVE_EXIF_INFO = False

    # enable this options to specify client-side cache in seconds
    MAX_AGE = 24 * 60 * 60

    # client-side caching time for temporary images (using queued detectors or after detection errors)
    MAX_AGE_TEMP_IMAGE = 0

    # Sends If-Modified-Since & Last-Modified headers; requires support from result storage
    SEND_IF_MODIFIED_LAST_MODIFIED_HEADERS = False

    # the way images are to be loaded
    # LOADER = 'thumbor.loaders.http_loader'
    # LOADER = 'thumbor.loaders.file_loader'

    S3_SERVER = ''
    S3_ACCESS_KEY = ''
    S3_SECRET_KEY = ''
    S3_REGION = ''
    S3_BUCKET_NAME = ''


    # If you want to cache results, use this options to specify how to cache it
    # Set Expiration seconds to ZERO if you want them not to expire.

    # stores the crypto key in each image in the storage
    # this is VERY useful to allow changing the security key
    STORES_CRYPTO_KEY_FOR_EACH_IMAGE = False

    # imaging engine to use to process images

    ENGINE = 'snipper.engines.pil'
    GIF_ENGINE = 'snipper.engines.gif'
    USE_GIFSICLE_ENGINE = False

    # if you need to use the OpenCV engine please refer
    # to https://github.com/thumbor/opencv-engine
    # ENGINE = 'opencv_engine'

    # if you need to use the GraphicsMagick engine please refer
    # to https://github.com/thumbor/graphicsmagick-engine
    # ENGINE = 'graphicsmagick_engine'

    # this is the security key used to encrypt/decrypt urls.
    # make sure this is unique and not well-known
    SECURITY_KEY = "MY_SECURE_KEY"

    # if you enable this, the unencryted URL will be available
    # to users.
    # IT IS VERY ADVISED TO SET THIS TO False TO STOP OVERLOADING
    # OF THE SERVER FROM MALICIOUS USERS
    ALLOW_UNSAFE_URL = True


    FILTERS = [
        # 'thumbor.filters.brightness',
        # 'thumbor.filters.colorize',
        # 'thumbor.filters.contrast',
        # 'thumbor.filters.rgb',
        # 'thumbor.filters.round_corner',
        # 'thumbor.filters.quality',
        # 'thumbor.filters.noise',
        # 'thumbor.filters.watermark',
        # 'thumbor.filters.equalize',
        # 'thumbor.filters.fill',
        # 'thumbor.filters.saturation',
        # 'thumbor.filters.sharpen',
        # 'thumbor.filters.strip_icc',
        # 'thumbor.filters.frame',
        # 'thumbor.filters.grayscale',
        # 'thumbor.filters.rotate',
        # 'thumbor.filters.format',
        # 'thumbor.filters.max_bytes',
        # 'thumbor.filters.no_upscale',
        'snipper.filters.download',
        'snipper.filters.expires',
        'snipper.filters.watermark',
        'snipper.filters.grayscale',
        ## can only be applied if there are already points for the image being served
        ## this means that either you are using the local face detector or the image
        ## has already went through remote detection
        ## 'thumbor.filters.redeye',
    ]

    # OPTIMIZERS = [
    # 'thumbor.optimizers.jpegtran',
    # 'thumbor.optimizers.gifv',
    # ]

    # JPEGTRAN_PATH = '/usr/local/bin/jpegtran'