from . import BaseFilter, filter_method
from datetime import datetime


class Filter(BaseFilter):
    @filter_method(BaseFilter.PositiveNumber)
    def expires(self, value):
        timestamp = int(value)
        expiry = datetime.utcfromtimestamp(timestamp)
        now = datetime.utcnow()
        has_expired = now > expiry
        self.context.request.has_expired = has_expired
