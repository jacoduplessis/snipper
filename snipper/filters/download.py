from . import BaseFilter, filter_method


class Filter(BaseFilter):

    @filter_method(BaseFilter.String)
    def download(self, value):
        self.context.request.download = str(value)
