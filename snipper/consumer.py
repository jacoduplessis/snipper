from django.http import HttpResponse
from channels.handler import AsgiHandler, AsgiRequest
import logging
from .handler import Handler

logger = logging.getLogger(__name__)


def get_image_consumer(config=None):

    def image_consumer(message):

        path = message.content['path']
        logger.debug(f"Image path requested: {path}")
        request = AsgiRequest(message)
        response = HttpResponse()
        handler = Handler(request=request, response=response, config=config)
        valid = handler.check_image()
        if not valid:
            logger.info(f"Invalid request: {path}")
            for chunk in AsgiHandler.encode_response(HttpResponse("Invalid request")):
                message.reply_channel.send(chunk)
            return

        handler.execute_image_operations()

        for chunk in AsgiHandler.encode_response(handler.response):
            message.reply_channel.send(chunk)

    return image_consumer
