#!/usr/bin/python
# -*- coding: utf-8 -*-

# thumbor imaging service
# https://github.com/thumbor/thumbor/wiki

# Licensed under the MIT license:
# http://www.opensource.org/licenses/mit-license
# Copyright (c) 2011 globo.com thumbor@googlegroups.com

import datetime
import logging
import re
from urllib.request import quote, unquote

from .context import Context, RequestParameters
from .engines import BaseEngine, EngineResult
from .engines.json_engine import JSONEngine
from .filters import PHASE_POST_TRANSFORM, PHASE_AFTER_LOAD, PHASE_PRE_LOAD
from .loader import load as s3_load, LoaderResult
from .signer import UrlSigner
from .transformer import Transformer
from .url import Url
from .utils import CONTENT_TYPE, EXTENSION, get_config, get_importer, get_context

HTTP_DATE_FMT = "%a, %d %b %Y %H:%M:%S GMT"

logger = logging.getLogger(__name__)


class FetchResult(object):
    def __init__(self, normalized=False, buffer=None, engine=None, successful=False, loader_error=None):
        self.normalized = normalized
        self.engine = engine
        self.buffer = buffer
        self.successful = successful
        self.loader_error = loader_error


class Handler:
    def __init__(self, request, response, config=None):

        self.request = request
        self.response = response

        if config is None:
            config = get_config()

        importer = get_importer(config)

        context = get_context(config, importer)

        request_params = RequestParameters(request=request, **Url.parse(request.path))

        self.context = Context(
            config=context.config,
            importer=context.modules.importer,
            request=request_params,
        )

        self.filters_runner = self.context.filters_factory.create_instances(self.context, self.context.request.filters)


    def set_status(self, status_code):
        self.response.status_code = status_code

    def get_status(self):
        return self.response.status_code

    def set_header(self, key, value):
        self.response[key] = value

    def _error(self, status, msg=None):
        self.set_status(status)
        if msg is not None:
            logger.warn(msg)

    def execute_image_operations(self):
        self.context.request.quality = None

        req = self.context.request
        conf = self.context.config

        if conf.MAX_WIDTH and (not isinstance(req.width, str)) and req.width > conf.MAX_WIDTH:
            req.width = conf.MAX_WIDTH
        if conf.MAX_HEIGHT and (not isinstance(req.height, str)) and req.height > conf.MAX_HEIGHT:
            req.height = conf.MAX_HEIGHT

        req.meta_callback = conf.META_CALLBACK_NAME or self.request.GET.get('callback', None)

        self.filters_runner.apply_filters(PHASE_PRE_LOAD)
        self.get_image()

    def get_image(self):
        """
        This function is called after the PRE_LOAD filters have been applied.
        It applies the AFTER_LOAD filters on the result, then crops the image.
        """

        try:
            result = self._fetch(
                self.context.request.image_url
            )

            self.result = result
            if not result.successful:
                if result.loader_error == LoaderResult.ERROR_NOT_FOUND:
                    self._error(404)
                    return
                elif result.loader_error == LoaderResult.ERROR_UPSTREAM:
                    # Return a Bad Gateway status if the error came from upstream
                    self._error(502)
                    return
                elif result.loader_error == LoaderResult.ERROR_TIMEOUT:
                    # Return a Gateway Timeout status if upstream timed out (i.e. 599)
                    self._error(504)
                    return
                elif hasattr(result, 'engine_error') and result.engine_error == EngineResult.COULD_NOT_LOAD_IMAGE:
                    self._error(400)
                    return
                else:
                    self._error(500)
                    return

        except Exception as e:
            msg = '[ThumborHandler] get_image failed for url `{url}`. error: `{error}`'.format(
                url=self.context.request.image_url,
                error=e
            )

            logger.warning(msg)
            return

        buffer = result.buffer
        engine = result.engine

        if engine is None:
            if buffer is None:
                self._error(504)
                return

            engine = self.context.request.engine
            try:
                engine.load(buffer, self.context.request.extension)
            except Exception:
                self._error(504)
                return

        self.context.transformer = Transformer(self.context)
        self.filters_runner.apply_filters(PHASE_AFTER_LOAD)
        self.transform()

    def transform(self):
        normalized = self.result.normalized
        req = self.context.request
        engine = self.result.engine

        self.normalize_crops(normalized, req, engine)

        if req.meta:
            self.context.transformer.engine = self.context.request.engine = JSONEngine(engine, req.image_url,
                                                                                       req.meta_callback)

        def dummy():
            pass

        self.context.transformer.transform(dummy)
        self.after_transform()

    def normalize_crops(self, normalized, req, engine):
        new_crops = None
        if normalized and req.should_crop:
            crop_left = req.crop['left']
            crop_top = req.crop['top']
            crop_right = req.crop['right']
            crop_bottom = req.crop['bottom']

            actual_width, actual_height = engine.size

            if not req.width and not req.height:
                actual_width = engine.size[0]
                actual_height = engine.size[1]
            elif req.width:
                actual_height = engine.get_proportional_height(engine.size[0])
            elif req.height:
                actual_width = engine.get_proportional_width(engine.size[1])

            new_crops = self.translate_crop_coordinates(
                engine.source_width,
                engine.source_height,
                actual_width,
                actual_height,
                crop_left,
                crop_top,
                crop_right,
                crop_bottom
            )
            req.crop['left'] = new_crops[0]
            req.crop['top'] = new_crops[1]
            req.crop['right'] = new_crops[2]
            req.crop['bottom'] = new_crops[3]

    def after_transform(self):

        if self.context.request.extension == '.gif' and self.context.config.USE_GIFSICLE_ENGINE:
            self.finish_request()
        else:
            self.filters_runner.apply_filters(PHASE_POST_TRANSFORM)
            self.finish_request()

    def is_webp(self, context):
        return (context.config.AUTO_WEBP and
                context.request.accepts_webp and
                not context.request.engine.is_multiple() and
                context.request.engine.can_convert_to_webp())

    def is_animated_gif(self, data):
        if data[:6] not in [b"GIF87a", b"GIF89a"]:
            return False
        i = 10  # skip header
        frames = 0

        def skip_color_table(i, flags):
            if flags & 0x80:
                i += 3 << ((flags & 7) + 1)
            return i

        flags = ord(data[i])
        i = skip_color_table(i + 3, flags)
        while frames < 2:
            block = data[i]
            i += 1
            if block == b'\x3B':
                break
            if block == b'\x21':
                i += 1
            elif block == b'\x2C':
                frames += 1
                i += 8
                i = skip_color_table(i + 1, ord(data[i]))
                i += 1
            else:
                return False
            while True:
                l = ord(data[i])
                i += 1
                if not l:
                    break
                i += l
        return frames > 1

    def define_image_type(self, context, result):
        if result is not None:

            buffer = result
            image_extension = EXTENSION.get(BaseEngine.get_mimetype(buffer), '.jpg')
        else:
            image_extension = context.request.format
            if image_extension is not None:
                image_extension = '.%s' % image_extension
                logger.debug('Image format specified as %s.' % image_extension)
            elif self.is_webp(context):
                image_extension = '.webp'
                logger.debug('Image format set by AUTO_WEBP as %s.' % image_extension)
            else:
                image_extension = context.request.engine.extension
                logger.debug('No image format specified. Retrieving from the image extension: %s.' % image_extension)

        content_type = CONTENT_TYPE.get(image_extension, CONTENT_TYPE['.jpg'])

        if context.request.meta:
            content_type = 'text/javascript' if context.request.meta_callback else 'application/json'
            logger.debug('Metadata requested. Serving content type of %s.' % content_type)

        logger.debug('Content Type of %s detected.' % content_type)

        return (image_extension, content_type)

    def _load_results(self, context):
        image_extension, content_type = self.define_image_type(context, None)

        quality = self.context.request.quality
        if quality is None:
            if image_extension == '.webp' and self.context.config.WEBP_QUALITY is not None:
                quality = self.context.config.get('WEBP_QUALITY')
            else:
                quality = self.context.config.QUALITY
        results = context.request.engine.read(image_extension, quality)
        if context.request.max_bytes is not None:
            results = self.reload_to_fit_in_kb(
                context.request.engine,
                results,
                image_extension,
                quality,
                context.request.max_bytes
            )
        if not context.request.meta:
            results = self.optimize(context, image_extension, results)
            # An optimizer might have modified the image format.
            content_type = BaseEngine.get_mimetype(results)

        return results, content_type

    def finish_request(self):
        context = self.context
        results, content_type = self._load_results(context)
        self._write_results_to_client(context, results, content_type)

    def _write_results_to_client(self, context, results, content_type):

        max_age = context.config.MAX_AGE

        if context.request.max_age is not None:
            max_age = context.request.max_age

        if context.request.prevent_result_storage or context.request.detection_error:
            max_age = context.config.MAX_AGE_TEMP_IMAGE

        if max_age:
            self.set_header('Cache-Control', 'max-age=' + str(max_age) + ',public')
            self.set_header('Expires', datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age))

        self.set_header('Content-Type', content_type)

        if hasattr(context.request, 'has_expired') and context.request.has_expired:
            self._error(410, 'Expired')
            self.set_header('Content-Type', 'text/html')
            results = b'expired'

        if hasattr(context.request, 'download'):
            self.set_header('Cache-Control', 'private')
            self.set_header('Content-Disposition', 'attachment; filename=' + context.request.download)

        buffer = results

        # auto-convert configured?
        should_vary = context.config.AUTO_WEBP
        # we have image (not video)
        should_vary = should_vary and content_type.startswith("image/")
        # output format is not requested via format filter
        should_vary = should_vary and not (
            context.request.format  # format is supported by filter
            and bool(re.search(r"format\([^)]+\)", context.request.filters))  # filter is in request
        )
        # our image is not animated gif
        should_vary = should_vary and not self.is_animated_gif(buffer)

        if should_vary:
            self.set_header('Vary', 'Accept')

        # context.headers = self._headers.copy()

        self._response_ext = EXTENSION.get(content_type)
        self._response_length = len(buffer)

        self.response.write(buffer)
        # self.finish()

    def optimize(self, context, image_extension, results):
        for optimizer in context.modules.optimizers:
            new_results = optimizer(context).run_optimizer(image_extension, results)
            if new_results is not None:
                results = new_results

        return results

    def reload_to_fit_in_kb(self, engine, initial_results, extension, initial_quality, max_bytes):
        if extension not in ['.webp', '.jpg', '.jpeg'] or len(initial_results) <= max_bytes:
            return initial_results

        results = initial_results
        quality = initial_quality

        while len(results) > max_bytes:
            quality = int(quality * 0.75)

            if quality < 10:
                logger.debug('Could not find any reduction that matches required size of %d bytes.' % max_bytes)
                return initial_results

            logger.debug('Trying to downsize image with quality of %d...' % quality)
            results = engine.read(extension, quality)

        prev_result = results
        while len(results) <= max_bytes:
            quality = int(quality * 1.1)
            logger.debug('Trying to upsize image with quality of %d...' % quality)
            prev_result = results
            results = engine.read(extension, quality)

        return prev_result

    @classmethod
    def translate_crop_coordinates(
            cls,
            original_width,
            original_height,
            width,
            height,
            crop_left,
            crop_top,
            crop_right,
            crop_bottom):

        if original_width == width and original_height == height:
            return

        crop_left = crop_left * width / original_width
        crop_top = crop_top * height / original_height

        crop_right = crop_right * width / original_width
        crop_bottom = crop_bottom * height / original_height

        return (crop_left, crop_top, crop_right, crop_bottom)

    def _fetch(self, url):
        """

        :param url:
        :type url:
        :return:
        :rtype:
        """
        fetch_result = FetchResult()

        mime = None

        loader_result = s3_load(self.context, url)

        if isinstance(loader_result, LoaderResult):
            # TODO _fetch should probably return a result object vs a list to
            # to allow returning metadata
            if not loader_result.successful:
                fetch_result.buffer = None
                fetch_result.loader_error = loader_result.error
                return fetch_result

            fetch_result.buffer = loader_result.buffer
        else:
            # Handle old loaders
            fetch_result.buffer = loader_result

        if fetch_result.buffer is None:
            return fetch_result

        fetch_result.successful = True

        if mime is None:
            mime = BaseEngine.get_mimetype(fetch_result.buffer)

        self.context.request.extension = extension = EXTENSION.get(mime, '.jpg')

        original_preserve = self.context.config.PRESERVE_EXIF_INFO
        self.context.config.PRESERVE_EXIF_INFO = True

        try:
            if mime == 'image/gif' and self.context.config.USE_GIFSICLE_ENGINE:
                self.context.request.engine = self.context.modules.gif_engine
            else:
                self.context.request.engine = self.context.modules.engine

            self.context.request.engine.load(fetch_result.buffer, extension)

            if self.context.request.engine.image is None:
                fetch_result.successful = False
                fetch_result.buffer = None
                fetch_result.engine = self.context.request.engine
                fetch_result.engine_error = EngineResult.COULD_NOT_LOAD_IMAGE
                return fetch_result

            fetch_result.normalized = self.context.request.engine.normalize()
        except Exception as e:
            print(e)
            fetch_result.successful = False
        finally:
            self.context.config.PRESERVE_EXIF_INFO = original_preserve
            fetch_result.buffer = None
            fetch_result.engine = self.context.request.engine
            return fetch_result

    def compute_etag(self):
        pass
        # import hashlib
        # hasher = hashlib.sha1()
        # for part in self._write_buffer:
        #     hasher.update(part)
        # return '"%s"' % hasher.hexdigest()

    def get_blacklist_contents(self):
        return []

    def check_image(self):

        url = self.request.path

        # if self.context.config.USE_BLACKLIST:
        #     blacklist = self.get_blacklist_contents()
        #     if self.context.request.image_url in blacklist:
        #         self._error(400, 'Source image url has been blacklisted: %s' % self.context.request.image_url)
        #         return False
        #

        if self.context.request.unsafe:
            if self.context.config.ALLOW_UNSAFE_URL:
                logger.debug("Unsafe request allowed")
                return True
            else:
                self._error(400, "Unsafe request received but not allowed")
                return False

        hash = self.context.request.hash

        if not hash and not self.context.config.ALLOW_UNSAFE_URL:
            self._error(400, "Only signed requests allowed but request contains no signature")
            return False

        if hash:
            signer = UrlSigner(self.context.config.SECURITY_KEY)

            try:
                quoted_hash = quote(hash)
            except KeyError:
                self._error(400, 'Invalid hash: %s' % self.context.request.hash)
                return False

            url_to_validate = url.replace('/%s/' % hash, '').replace('/%s/' % quoted_hash, '')

            valid = signer.validate(unquote(hash).encode('utf-8'), url_to_validate)

            if not valid:
                self._error(400, 'Malformed URL: %s' % url)
                return False

            return True

        return False
