import base64
import hashlib
import hmac


class BaseUrlSigner:
    def __init__(self, security_key):
        if isinstance(security_key, str):
            security_key = security_key.encode('utf-8')
        self.security_key = security_key

    def validate(self, actual_signature, url):
        url_signature = self.signature(url)
        return url_signature == actual_signature

    def signature(self, url):
        raise NotImplementedError()


class UrlSigner(BaseUrlSigner):
    """Validate urls and sign them using base64 hmac-sha1
    """

    def signature(self, url):
        return base64.urlsafe_b64encode(
            hmac.new(
                self.security_key, str(url).encode('utf-8'), hashlib.sha1
            ).digest()
        )
