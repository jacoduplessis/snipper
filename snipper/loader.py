#!/usr/bin/python
# -*- coding: utf-8 -*-

from minio import Minio
from minio.error import NoSuchKey

class LoaderResult:

    ERROR_NOT_FOUND = 'not_found'
    ERROR_UPSTREAM = 'upstream'
    ERROR_TIMEOUT = 'timeout'

    def __init__(self, buffer=None, successful=True, error=None, metadata=dict()):
        '''
        :param buffer: The media buffer

        :param successful: True when the media has been loaded.
        :type successful: bool

        :param error: Error code
        :type error: str

        :param metadata: Dictionary of metadata about the buffer
        :type metadata: dict
        '''

        self.buffer = buffer
        self.successful = successful
        self.error = error
        self.metadata = metadata


def load(context, path):

    client = Minio(
        context.config.S3_SERVER,
        access_key=context.config.S3_ACCESS_KEY,
        secret_key=context.config.S3_SECRET_KEY,
        secure=False,
        region=context.config.S3_REGION,

    )

    result = LoaderResult()

    try:
        response = client.get_object(
            bucket_name=context.config.S3_BUCKET_NAME,
            object_name=path,
        )
        result.successful = True
        result.buffer = response.data
    except NoSuchKey:
        result.successful = False
        result.error = LoaderResult.ERROR_NOT_FOUND

    return result

